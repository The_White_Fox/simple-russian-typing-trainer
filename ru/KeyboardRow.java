/* 
 * Copyright 2023 Oliver Lenz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.oliverlenz;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Oli
 */
public enum KeyboardRow {
	NUMROW(Arrays.asList(
	new String("ё".getBytes(), Charset.forName("UTF-8")), 
	"0", "1", "2", "3", "4", "5", "6", "7", "8", "9")),
	UPPERROW(Arrays.asList(
	new String("й".getBytes(), Charset.forName("UTF-8")), 
	new String("ц".getBytes(), Charset.forName("UTF-8")), 
	new String("у".getBytes(), Charset.forName("UTF-8")), 
	new String("к".getBytes(), Charset.forName("UTF-8")), 
	new String("е".getBytes(), Charset.forName("UTF-8")), 
	new String("н".getBytes(), Charset.forName("UTF-8")), 
	new String("г".getBytes(), Charset.forName("UTF-8")), 
	new String("ш".getBytes(), Charset.forName("UTF-8")), 
	new String("щ".getBytes(), Charset.forName("UTF-8")), 
	new String("з".getBytes(), Charset.forName("UTF-8")), 
	new String("х".getBytes(), Charset.forName("UTF-8")), 
	new String("ъ".getBytes(), Charset.forName("UTF-8")))),
	MIDDLEROW(Arrays.asList(
	new String("ф".getBytes(), Charset.forName("UTF-8")), 
	new String("ы".getBytes(), Charset.forName("UTF-8")), 
	new String("в".getBytes(), Charset.forName("UTF-8")), 
	new String("а".getBytes(), Charset.forName("UTF-8")), 
	new String("п".getBytes(), Charset.forName("UTF-8")), 
	new String("р".getBytes(), Charset.forName("UTF-8")), 
	new String("о".getBytes(), Charset.forName("UTF-8")), 
	new String("л".getBytes(), Charset.forName("UTF-8")), 
	new String("д".getBytes(), Charset.forName("UTF-8")), 
	new String("ж".getBytes(), Charset.forName("UTF-8")), 
	new String("э".getBytes(), Charset.forName("UTF-8")))),
	LOWERROW(Arrays.asList(
	new String("я".getBytes(), Charset.forName("UTF-8")), 
	new String("ч".getBytes(), Charset.forName("UTF-8")), 
	new String("с".getBytes(), Charset.forName("UTF-8")), 
	new String("м".getBytes(), Charset.forName("UTF-8")), 
	new String("и".getBytes(), Charset.forName("UTF-8")), 
	new String("т".getBytes(), Charset.forName("UTF-8")), 
	new String("ь".getBytes(), Charset.forName("UTF-8")), 
	new String("б".getBytes(), Charset.forName("UTF-8")), 
	new String("ю".getBytes(), Charset.forName("UTF-8"))));
	
	private final List<String> characters;
	
	private KeyboardRow(List<String> characters) {
		this.characters = characters;
	}
	
	public Set<String> characters(){
		return new HashSet<>(characters);
	}
}
