/* 
 * Copyright 2023 Oliver Lenz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.oliverlenz;

import imgui.ImFontConfig;
import imgui.ImFontGlyphRangesBuilder;
import imgui.ImGui;
import imgui.ImGuiIO;
import imgui.ImGuiInputTextCallbackData;
import imgui.ImGuiViewport;
import imgui.callback.ImGuiInputTextCallback;
import imgui.flag.ImGuiConfigFlags;
import imgui.flag.ImGuiInputTextFlags;
import imgui.gl3.ImGuiImplGl3;
import imgui.glfw.ImGuiImplGlfw;
import imgui.type.ImBoolean;
import imgui.type.ImString;
import java.nio.charset.Charset;
import java.util.ResourceBundle;
import org.lwjgl.glfw.Callbacks;
import org.lwjgl.glfw.GLFW;
import static org.lwjgl.glfw.GLFW.GLFW_CONTEXT_VERSION_MAJOR;
import static org.lwjgl.glfw.GLFW.GLFW_CONTEXT_VERSION_MINOR;
import static org.lwjgl.glfw.GLFW.GLFW_FALSE;
import static org.lwjgl.glfw.GLFW.GLFW_RESIZABLE;
import static org.lwjgl.glfw.GLFW.glfwCreateWindow;
import static org.lwjgl.glfw.GLFW.glfwDestroyWindow;
import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.glfw.GLFW.glfwShowWindow;
import static org.lwjgl.glfw.GLFW.glfwSwapInterval;
import static org.lwjgl.glfw.GLFW.glfwTerminate;
import static org.lwjgl.glfw.GLFW.glfwWindowHint;
import static org.lwjgl.glfw.GLFW.glfwWindowShouldClose;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.opengl.GL;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.system.MemoryUtil.NULL;

/**
 * Window for ImGui, see:
 * https://blog.conan.io/2019/06/26/An-introduction-to-the-Dear-ImGui-library.html
 * https://www.youtube.com/watch?v=Xq-eVcNtUbw
 */
public class View extends ImGuiInputTextCallback {

	private ResourceBundle text;
	private ImGuiImplGlfw imGuiGlfw;
	private ImGuiImplGl3 imGuiGl3;
	private final String glslVersion = "#version 130";
	private final int GLFW_MAJOR = 3;
	private final int GLFW_MINOR = 0;
	private long windowPointer;

	private float windowsSizeWidth = 500;
	private float windowsSizeHeight = 250;

	private TypingTrainer typingTrainer;

	private ImBoolean numRowEnabled;
	private ImBoolean upperRowEnabled;
	private ImBoolean middleRowEnabled;
	private ImBoolean lowerRowEnabled;
	
	private int[] fingermode = new int[1];
	private int[] skillmode = new int[1];
	private int[] fingermodeOld = new int[1];
	private int[] skillmodeOld = new int[1];

	public View(TypingTrainer typingTrainer) {
		this.typingTrainer = typingTrainer;

		numRowEnabled = new ImBoolean(typingTrainer.isNumRowEnabled());
		upperRowEnabled = new ImBoolean(typingTrainer.isUpperRowEnabled());
		middleRowEnabled = new ImBoolean(typingTrainer.isMiddleRowEnabled());
		lowerRowEnabled = new ImBoolean(typingTrainer.isLowerRowEnabled());
		
		fingermode[0] = typingTrainer.getFingermode();
		fingermodeOld[0] = typingTrainer.getFingermode();
		
		skillmode[0] = typingTrainer.getSkillmode();
		skillmodeOld[0] = typingTrainer.getSkillmode();
	}

	public void init() {
		this.text = ResourceBundle.getBundle("de.oliverlenz.text");
		imGuiGlfw = new ImGuiImplGlfw();
		imGuiGl3 = new ImGuiImplGl3();

		initWindow();
		initGUI();

		imGuiGlfw.init(windowPointer, true);
		imGuiGl3.init(glslVersion);
	}

	public void destroy() {
		imGuiGl3.dispose();
		imGuiGlfw.dispose();
		ImGui.destroyContext();
		Callbacks.glfwFreeCallbacks(windowPointer);
		glfwDestroyWindow(windowPointer);
		glfwTerminate();
	}

	private void initWindow() {
		GLFWErrorCallback.createPrint(System.err).set();
		if (!glfwInit()) {
			System.out.println("Unable to initialize GLFW");
			System.exit(-1);
		}

		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, GLFW_MAJOR);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, GLFW_MINOR);
//		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
		glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

		windowPointer = glfwCreateWindow((int) windowsSizeWidth, (int) windowsSizeHeight, "Russian Typing Trainer", NULL, NULL);
		if (windowPointer == NULL) {
			System.out.println("Unable to create window");
			System.exit(-1);
		}

		glfwMakeContextCurrent(windowPointer);
		glfwSwapInterval(1);
		glfwShowWindow(windowPointer);

		GL.createCapabilities();
	}

	private void initGUI() {
		ImGui.createContext();
		
		//Init Fonts
		ImGuiIO imGuiIO = ImGui.getIO();
		imGuiIO.getFonts().addFontDefault(); // Add default font for latin glyphs
		
		ImFontGlyphRangesBuilder rangesBuilder = new ImFontGlyphRangesBuilder(); // Glyphs ranges provide
        rangesBuilder.addRanges(imGuiIO.getFonts().getGlyphRangesDefault());
        rangesBuilder.addRanges(imGuiIO.getFonts().getGlyphRangesCyrillic());
		
		ImFontConfig fontConfig = new ImFontConfig();
        fontConfig.setMergeMode(true);  // Enable merge mode to merge cyrillic
		
		short[] glyphRanges = rangesBuilder.buildRanges();
		imGuiIO.getFonts().addFontFromFileTTF("C:\\Windows\\Fonts\\Tahoma.ttf", 14, fontConfig, glyphRanges);
//        imGuiIO.getFonts().addFontFromMemoryTTF("C:\\Windows\\Fonts\\Tahoma.ttf".getBytes(), 14, fontConfig, glyphRanges); // cyrillic glyphs
		imGuiIO.getFonts().build();

        fontConfig.destroy();
	}
	
	public void show() {

		while (!glfwWindowShouldClose(windowPointer)) {
			glClearColor(0.1f, 0.09f, 0.1f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT);
			GLFW.glfwSetWindowSize(windowPointer, (int) windowsSizeWidth, (int) windowsSizeHeight);

			imGuiGlfw.newFrame();
			ImGui.newFrame();

			ImGuiViewport vp = ImGui.getMainViewport();
			ImGui.setNextWindowPos(vp.getPosX(), vp.getPosY());
			ImGui.setNextWindowSize(vp.getSizeX(), vp.getSizeY());

			paintImGui();

			ImGui.render();
			imGuiGl3.renderDrawData(ImGui.getDrawData());

			if (ImGui.getIO().hasConfigFlags(ImGuiConfigFlags.ViewportsEnable)) {
				final long backupWindowPtr = org.lwjgl.glfw.GLFW.glfwGetCurrentContext();
				ImGui.updatePlatformWindows();
				ImGui.renderPlatformWindowsDefault();
				GLFW.glfwMakeContextCurrent(backupWindowPtr);
			}

			GLFW.glfwSwapBuffers(windowPointer);
			GLFW.glfwPollEvents();
		}
	}

	@SuppressWarnings("empty-statement")
	private void paintImGui() {
		ImGui.begin(text.getString("1"), new ImBoolean(false));

		ImGui.setCursorPos(10, 30);
		ImGui.text(typingTrainer.textToType());
//		ImGui.text(new String("Привиет".getBytes(), Charset.forName("UTF-8")));
//		ImGui.dummy(0, 30);

		ImString s = new ImString();
		ImGui.inputText("##", s, ImGuiInputTextFlags.CallbackEdit , this);

		ImGui.setCursorPos(10, 90);
		if(ImGui.checkbox(text.getString("2"), numRowEnabled)){
			typingTrainer.enableNumRow(numRowEnabled.get());
		}

		ImGui.setCursorPos(10, 120);
		if(ImGui.checkbox(text.getString("3"), upperRowEnabled)){
			typingTrainer.enableUpperRow(upperRowEnabled.get());
		}

		ImGui.setCursorPos(10, 150);
		if(ImGui.checkbox(text.getString("4"), middleRowEnabled)){
			typingTrainer.enableMiddleRow(middleRowEnabled.get());
		}

		ImGui.setCursorPos(10, 180);
		if(ImGui.checkbox(text.getString("5"), lowerRowEnabled)){
			typingTrainer.enableLowerRow(lowerRowEnabled.get());
		}

		ImGui.setCursorPos(200, 90);
		ImGui.setNextItemWidth(100);
		ImGui.sliderInt(text.getString("6"), skillmode, 1, 3);
		if(skillmode[0] != skillmodeOld[0]){
			typingTrainer.setSkillmode(skillmode[0]);
		}
		skillmodeOld[0] = skillmode[0];

		ImGui.setCursorPos(200, 120);
		ImGui.setNextItemWidth(100);
		ImGui.sliderInt(text.getString("7"), fingermode, 2, 5);
		if(fingermode[0] != fingermodeOld[0]){
			typingTrainer.setFingermode(fingermode[0]);
		}
		fingermodeOld[0] = fingermode[0];
		
		ImGui.setCursorPos(200, 150);
		ImGui.text(text.getString("8") + " " + typingTrainer.getErrorCnt());

		ImGui.setCursorPos(200, 180);
		ImGui.text(text.getString("9") + " " + typingTrainer.getSpeedCnt() + "/min");
		ImGui.end();
	}

	@Override
	public void accept(ImGuiInputTextCallbackData t) {
		typingTrainer.feedWithUserInput(new String(t.getBuf().getBytes(), Charset.forName("UTF-8")));
		t.deleteChars(0, t.getBuf().getBytes(Charset.forName("UTF-8")).length);
	}
}
