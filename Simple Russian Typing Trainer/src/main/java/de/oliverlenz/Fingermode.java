/* 
 * Copyright 2023 Oliver Lenz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.oliverlenz;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Oli
 */
public enum Fingermode {
	WITH_INDEXFINGER(Arrays.asList(
	new String("5".getBytes(), Charset.forName("UTF-8")), 
	new String("6".getBytes(), Charset.forName("UTF-8")), 
	new String("7".getBytes(), Charset.forName("UTF-8")), 
	new String("8".getBytes(), Charset.forName("UTF-8")), 
	new String("к".getBytes(), Charset.forName("UTF-8")), 
	new String("е".getBytes(), Charset.forName("UTF-8")), 
	new String("н".getBytes(), Charset.forName("UTF-8")),
	new String("г".getBytes(), Charset.forName("UTF-8")),
	new String("а".getBytes(), Charset.forName("UTF-8")),
	new String("п".getBytes(), Charset.forName("UTF-8")),
	new String("р".getBytes(), Charset.forName("UTF-8")),
	new String("о".getBytes(), Charset.forName("UTF-8")),
	new String("м".getBytes(), Charset.forName("UTF-8")),
	new String("и".getBytes(), Charset.forName("UTF-8")),
	new String("т".getBytes(), Charset.forName("UTF-8")),
	new String("ь".getBytes(), Charset.forName("UTF-8")))),
	WITH_MIDDLEFINGER(Arrays.asList(
	new String("4".getBytes(), Charset.forName("UTF-8")), 
	new String("9".getBytes(), Charset.forName("UTF-8")), 
	new String("у".getBytes(), Charset.forName("UTF-8")), 
	new String("ш".getBytes(), Charset.forName("UTF-8")), 
	new String("в".getBytes(), Charset.forName("UTF-8")), 
	new String("л".getBytes(), Charset.forName("UTF-8")), 
	new String("с".getBytes(), Charset.forName("UTF-8")), 
	new String("б".getBytes(), Charset.forName("UTF-8")))),
	WITH_RINGFINGER(Arrays.asList(
	new String("3".getBytes(), Charset.forName("UTF-8")), 
	new String("0".getBytes(), Charset.forName("UTF-8")), 
	new String("ц".getBytes(), Charset.forName("UTF-8")), 
	new String("щ".getBytes(), Charset.forName("UTF-8")), 
	new String("ы".getBytes(), Charset.forName("UTF-8")), 
	new String("д".getBytes(), Charset.forName("UTF-8")), 
	new String("ч".getBytes(), Charset.forName("UTF-8")), 
	new String("ю".getBytes(), Charset.forName("UTF-8")))),
	WITH_PINKY(Arrays.asList(
	new String("ё".getBytes(), Charset.forName("UTF-8")), 
	new String("1".getBytes(), Charset.forName("UTF-8")), 
	new String("2".getBytes(), Charset.forName("UTF-8")), 
	new String("й".getBytes(), Charset.forName("UTF-8")), 
	new String("з".getBytes(), Charset.forName("UTF-8")), 
	new String("х".getBytes(), Charset.forName("UTF-8")), 
	new String("ъ".getBytes(), Charset.forName("UTF-8")), 
	new String("ф".getBytes(), Charset.forName("UTF-8")), 
	new String("ж".getBytes(), Charset.forName("UTF-8")), 
	new String("э".getBytes(), Charset.forName("UTF-8")), 
	new String("я".getBytes(), Charset.forName("UTF-8"))));

	private final List<String> characters;
	
	private Fingermode(List<String> characters) {
		this.characters = characters;
	}
	
	public Set<String> characters(){
		return new HashSet<>(characters);
	}

	public static Fingermode fromInt(int i) {
		switch (i) {
			case 2:
				return WITH_INDEXFINGER;
			case 3:
				return WITH_MIDDLEFINGER;
			case 4:
				return WITH_RINGFINGER;
			case 5:
				return WITH_PINKY;
			default:
				throw new RuntimeException("Not existing Fingermode requested");
		}
	}
	
	public static int fromFingermode(Fingermode fm){
		switch (fm) {
			case WITH_INDEXFINGER:
				return 2;
			case WITH_MIDDLEFINGER:
				return 3;
			case WITH_RINGFINGER:
				return 4;
			case WITH_PINKY:
				return 5;
			default:
				throw new RuntimeException("Not existing Fingermode requested");
		}
	}
}
