/* 
 * Copyright 2023 Oliver Lenz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.oliverlenz;

import static de.oliverlenz.Fingermode.WITH_INDEXFINGER;
import static de.oliverlenz.Fingermode.WITH_MIDDLEFINGER;
import static de.oliverlenz.Fingermode.WITH_PINKY;
import static de.oliverlenz.Fingermode.WITH_RINGFINGER;
import java.util.HashSet;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 */
public class TypingTrainer {

	private boolean isNumRowEnabled;
	private boolean isUpperRowEnabled;
	private boolean isMiddleRowEnabled;
	private boolean isLowerRowEnabled;
	private Skillmode skillmode;
	private Fingermode fingermode;

	private String nextCharactersequenceToType = "";
	private String inputBuffer = "";
	private HashSet<String> trainingCharacters;
	private final int minimumSequenceLength = 20;
	
	private int errors = 0;
	private int speedCount = 0;
	private int calculatedTypeSpeed = 0;
	private Timer typeSpeedTimer;
	private final long typeSpeedIntervall = 20000; //Time in ms for type speed measurement

	public TypingTrainer() {
		isNumRowEnabled = false;
		isUpperRowEnabled = false;
		isMiddleRowEnabled = true;
		isLowerRowEnabled = false;
		skillmode = Skillmode.GREENHORN;
		fingermode = Fingermode.WITH_INDEXFINGER;

		trainingCharacters = new HashSet<>();
		refreshTrainingCharacters();
		refreshTypeSequence();
		
		setTypeSpeedTimer();
	}
	
	private void setTypeSpeedTimer() {
		typeSpeedTimer = new Timer();
		typeSpeedTimer.scheduleAtFixedRate(new TimerTask() {
			private long speedCorrectionFactor = 60000 / typeSpeedIntervall;
			
			@Override
			public void run() {
				calculatedTypeSpeed = (int) (speedCount * speedCorrectionFactor);
				speedCount = 0;
			}
		}, 0, typeSpeedIntervall);
	}

	String textToType() {
		return nextCharactersequenceToType;
	}

	void feedWithUserInput(String userInput) {
		filterFormatAndConcat(userInput);
		processUserInput();
	}

	private void filterFormatAndConcat(String userInput) {
		userInput = userInput.toLowerCase();
		userInput = userInput.replaceAll("[^[ёа-я]|[0-9]|[^\b]]", "");
		//userInput = userInput.replaceAll("[^[a-z]|[0-9]|[^\b]]", "");
		
		if(!userInput.equals("")){
			speedCount = speedCount + userInput.length();
			inputBuffer = inputBuffer.concat(userInput);
		}
	}

	private void processUserInput() {
		while (!inputBuffer.isEmpty()) {
			String nextInputCharacter = inputBuffer.substring(0, 1);
			inputBuffer = inputBuffer.substring(1, inputBuffer.length());

			if (nextInputCharacter.equals(nextCharactersequenceToType.substring(0, 1))) {
				nextCharactersequenceToType = nextCharactersequenceToType.substring(1, nextCharactersequenceToType.length());
				if(nextCharactersequenceToType.length() < minimumSequenceLength){
				refreshTypeSequence();
				}
			}
			else{
				errors++;
			}
		}
	}

	private void refreshTypeSequence() {
		while (nextCharactersequenceToType.length() < minimumSequenceLength) {
			String quatrupel = "";
			switch (skillmode) {
				case GREENHORN:
					quatrupel = addSimpleQuatriple();
					break;
				case MIDDLE:
					quatrupel = addMiddleQuatriple();
					break;
				case REALISTIC:
					quatrupel = addRealisticQuatriple();
			}
			if (nextCharactersequenceToType.isEmpty()) {
				nextCharactersequenceToType = nextCharactersequenceToType.concat(quatrupel);
			}
			else {
				nextCharactersequenceToType = nextCharactersequenceToType.concat(" " + quatrupel);
			}
		}
	}

	private String addSimpleQuatriple() {
		String c = getRandomCharacter();
		return c + c + c + c;
	}

	private String addMiddleQuatriple() {
		String c1 = getRandomCharacter();
		String c2 = getRandomCharacter();
		return c1 + c1 + c2 + c2;
	}

	private String addRealisticQuatriple() {
		String quatrupel = "";
		for (int i = 0; i < 4; i++) {
			quatrupel = quatrupel + getRandomCharacter();
		}
		return quatrupel;
	}

	private String getRandomCharacter() {
		var random = new Random().nextInt(trainingCharacters.size());
		int element = 0;
		for (String s : trainingCharacters) {
			if (element == random) {
				return s;
			}
			element++;
		}
		throw new RuntimeException("Bad random");
	}

	private void refreshTrainingCharacters() {
		HashSet<String> enabledCharactersByKeyboardRows = new HashSet<>();
		if (isNumRowEnabled) {
			enabledCharactersByKeyboardRows.addAll(KeyboardRow.NUMROW.characters());
		}
		if (isUpperRowEnabled) {
			enabledCharactersByKeyboardRows.addAll(KeyboardRow.UPPERROW.characters());
		}
		if (isMiddleRowEnabled) {
			enabledCharactersByKeyboardRows.addAll(KeyboardRow.MIDDLEROW.characters());
		}
		if (isLowerRowEnabled) {
			enabledCharactersByKeyboardRows.addAll(KeyboardRow.LOWERROW.characters());
		}

		HashSet<String> enabledCharactersByFingerselection = new HashSet<>();
		switch (fingermode) {
			case WITH_INDEXFINGER:
				enabledCharactersByFingerselection.addAll(Fingermode.WITH_INDEXFINGER.characters());
				break;
			case WITH_MIDDLEFINGER:
				enabledCharactersByFingerselection.addAll(Fingermode.WITH_INDEXFINGER.characters());
				enabledCharactersByFingerselection.addAll(Fingermode.WITH_MIDDLEFINGER.characters());
				break;
			case WITH_RINGFINGER:
				enabledCharactersByFingerselection.addAll(Fingermode.WITH_INDEXFINGER.characters());
				enabledCharactersByFingerselection.addAll(Fingermode.WITH_MIDDLEFINGER.characters());
				enabledCharactersByFingerselection.addAll(Fingermode.WITH_RINGFINGER.characters());
				break;
			case WITH_PINKY:
				enabledCharactersByFingerselection.addAll(Fingermode.WITH_INDEXFINGER.characters());
				enabledCharactersByFingerselection.addAll(Fingermode.WITH_MIDDLEFINGER.characters());
				enabledCharactersByFingerselection.addAll(Fingermode.WITH_RINGFINGER.characters());
				enabledCharactersByFingerselection.addAll(Fingermode.WITH_PINKY.characters());
				break;
		}
		enabledCharactersByFingerselection.retainAll(enabledCharactersByKeyboardRows);
		trainingCharacters = enabledCharactersByFingerselection;
	}

	public boolean isNumRowEnabled() {
		return isNumRowEnabled;
	}

	public boolean isUpperRowEnabled() {
		return isUpperRowEnabled;
	}

	public boolean isMiddleRowEnabled() {
		return isMiddleRowEnabled;
	}

	public boolean isLowerRowEnabled() {
		return isLowerRowEnabled;
	}

	void enableNumRow(boolean numRowState) {
		isNumRowEnabled = numRowState;
		refreshTrainingCharacters();
	}

	void enableUpperRow(boolean upperRowState) {
		isUpperRowEnabled = upperRowState;
		refreshTrainingCharacters();
	}

	void enableMiddleRow(boolean middleRowState) {
		isMiddleRowEnabled = middleRowState;
		refreshTrainingCharacters();
	}

	void enableLowerRow(boolean lowerRowState) {
		isLowerRowEnabled = lowerRowState;
		refreshTrainingCharacters();
	}

	void setFingermode(int i) {
		fingermode = fingermode.fromInt(i);
		refreshTrainingCharacters();
	}

	int getFingermode() {
		return Fingermode.fromFingermode(fingermode);
	}

	void setSkillmode(int i) {
		skillmode = skillmode.fromInt(i);
		refreshTrainingCharacters();
	}

	int getSkillmode() {
		return Skillmode.fromSkillmode(skillmode);
	}
	
	public int getErrorCnt(){
		return errors;
	}
	
	public int getSpeedCnt(){
		return calculatedTypeSpeed;
	}

	void terminate() {
		typeSpeedTimer.cancel();
		typeSpeedTimer.purge();
		typeSpeedTimer = null;
	}
}
