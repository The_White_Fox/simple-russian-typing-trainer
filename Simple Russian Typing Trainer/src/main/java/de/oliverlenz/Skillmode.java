/* 
 * Copyright 2023 Oliver Lenz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.oliverlenz;

/**
 *
 * @author Oli
 */
public enum Skillmode {
	GREENHORN,
	MIDDLE,
	REALISTIC;

	public static Skillmode fromInt(int i) {
		switch (i) {
			case 1:
				return GREENHORN;
			case 2:
				return MIDDLE;
			case 3:
				return REALISTIC;
			default:
				throw new RuntimeException("Not existing skillmode requested.");
		}
	}

	public static int fromSkillmode(Skillmode sk) {
		switch (sk) {
			case GREENHORN:
				return 1;
			case MIDDLE:
				return 2;
			case REALISTIC:
				return 3;
			default:
				throw new RuntimeException("Not existing skillmode requested.");
		}
	}
}
