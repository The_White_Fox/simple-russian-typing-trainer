Name "Simple Russian Typing Trainer"

!define APPNAME "Simple Russian Typing Trainer"

!makensis "GetVersion.nsi"
!system "GetVersion.exe"
!include "nsisVersion.txt"
#cleanup
#!delfile "GetVersion.exe"
#!delfile "nsisVersion.txt"

Page license
Page directory
#Page components
Page instfiles
UninstPage uninstConfirm
UninstPage instfiles


LicenseData "Licence.txt"

OutFile "${APPNAME} ${Version}.exe"

InstallDir "$PROGRAMFILES64\${APPNAME}"

Section "Java"
	SetOutPath $INSTDIR
	MessageBox MB_YESNO "Install Java 21?" /SD IDYES IDNO endInstallJava
	File "OpenJDK21U-jdk_x64_windows_hotspot_21.0.1_12.msi"
	ExecWait  '"msiexec" /i "$INSTDIR\OpenJDK21U-jdk_x64_windows_hotspot_21.0.1_12.msi"'
	Delete "$INSTDIR\OpenJDK21U-jdk_x64_windows_hotspot_21.0.1_12.msi"
	endInstallJava:

	SetOutPath $INSTDIR
SectionEnd

Section "Simple Russian Typing Trainer"
	MessageBox MB_OK "You need to add Russian language to your system. (Sorry that I can't do.)"
	ExecShell "open" "https://support.microsoft.com/de-de/windows/verwalten-der-eingabe-und-anzeigespracheinstellungen-in-windows-12a10cb4-8626-9b77-0ccb-5013e0c7c7a2" SW_SHOWNORMAL
	File /r "Simple Russian Typing Trainer\build\distributions\Russian Typing Trainer Windows-1.0.0\*"
	WriteUninstaller $INSTDIR\uninstall.exe
SectionEnd

Section "Start Menu Shortcuts"
	MessageBox MB_YESNO "Create Startmenu Shortcuts?" /SD IDYES IDNO endShortcuts
	CreateDirectory "$SMPROGRAMS\${APPNAME}"
	CreateShortCut "$SMPROGRAMS\${APPNAME}\Uninstall.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0
	CreateShortCut "$SMPROGRAMS\${APPNAME}\${APPNAME}.lnk" "$INSTDIR\bin\Simple Russian Typing Trainer.bat" "" "$INSTDIR\bin\Simple Russian Typing Trainer.bat" 0
	endShortcuts:
SectionEnd
	

Section "Uninstall"
	RMDir /r "$INSTDIR"
	RMDir /r "$SMPROGRAMS\${APPNAME}"
SectionEnd
