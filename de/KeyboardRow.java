/* 
 * Copyright 2023 Oliver Lenz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.oliverlenz;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Oli
 */
public enum KeyboardRow {
	NUMROW(Arrays.asList("0", "1", "2", "3", "4", "5", "6", "7", "8", "9")),
	UPPERROW(Arrays.asList("q", "w", "e", "r", "t", "z", "u", "i", "o", "p", "�")),
	MIDDLEROW(Arrays.asList("a", "s", "d", "f", "g", "h", "j", "k", "l", "�", "�")),
	LOWERROW(Arrays.asList("y", "x", "c", "v", "b", "n", "m"));
	
	private final List<String> characters;
	
	private KeyboardRow(List<String> characters) {
		this.characters = characters;
	}
	
	public Set<String> characters(){
		return new HashSet<>(characters);
	}
}
