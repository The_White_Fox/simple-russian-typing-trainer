/* 
 * Copyright 2023 Oliver Lenz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.oliverlenz;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Oli
 */
public enum Fingermode {
	WITH_INDEXFINGER(Arrays.asList("5", "6", "7", "8", "r", "t", "z", 
			"u", "f", "g", "h", "j", "v", "b", "n", "m")),
	WITH_MIDDLEFINGER(Arrays.asList("4", "9", "e", "i", "d", "k", "c")),
	WITH_RINGFINGER(Arrays.asList("3", "0", "w", "o", "s", "l", "x")),
	WITH_PINKY(Arrays.asList("1", "2", "q", "p", "a"));

	private final List<String> characters;
	
	private Fingermode(List<String> characters) {
		this.characters = characters;
	}
	
	public Set<String> characters(){
		return new HashSet<>(characters);
	}

	public static Fingermode fromInt(int i) {
		switch (i) {
			case 2:
				return WITH_INDEXFINGER;
			case 3:
				return WITH_MIDDLEFINGER;
			case 4:
				return WITH_RINGFINGER;
			case 5:
				return WITH_PINKY;
			default:
				throw new RuntimeException("Not existing Fingermode requested");
		}
	}
	
	public static int fromFingermode(Fingermode fm){
		switch (fm) {
			case WITH_INDEXFINGER:
				return 2;
			case WITH_MIDDLEFINGER:
				return 3;
			case WITH_RINGFINGER:
				return 4;
			case WITH_PINKY:
				return 5;
			default:
				throw new RuntimeException("Not existing Fingermode requested");
		}
	}
}
