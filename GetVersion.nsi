!define File "GetVersion.exe"

OutFile "GetVersion.exe"
SilentInstall silent
RequestExecutionLevel user ; don't write $EXEDIR\Version.txt with admin permissions and prevent invoking UAC
 
Section
	#Get file version
	ClearErrors
	FileOpen $1 "version.txt" r
	FileSeek $1 0
	FileRead $1 $2
	FileClose $1
 
	## Write it to a !define for use in main script
	FileOpen $R0 "$EXEDIR\nsisVersion.txt" w
	FileWrite $R0 '!define Version "$2"'
	FileClose $R0
 
SectionEnd
